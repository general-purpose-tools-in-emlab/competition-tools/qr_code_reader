# 'qr_code_reader' Package

The `qr_code_reader` repository is to read QR code by a camera.  
In addition, this package is implemented based on ROS.

*   Maintainer: Shoichi Hasegawa ([hasegawa.shoichi@em.ci.ritsumei.ac.jp](mailto:hasegawa.shoichi@em.ci.ritsumei.ac.jp)).
*   Author: Shoichi Hasegawa ([hasegawa.shoichi@em.ci.ritsumei.ac.jp](mailto:hasegawa.shoichi@em.ci.ritsumei.ac.jp)).

**Content:**

*   [Setup](#Setup)
*   [Subscribed Topics](#subscribedtopics)
*   [Launch](#launch)
*   [Files](#files)
*   [References](#References)


## Setup
~~~
python3 -m pip install -r requirements.txt
~~~

~~~
apt-get install python3-zbar
~~~

<!--
# ==================================================================================================
#   Subscribed Topics
# ==================================================================================================
--->

## Subscribed Topics

* **`/input/image/compressed`** ([sensor_msgs/CompressedImage])  
  Subscribe to an image.


## How to use

ROS-based program  
~~~
roslaunch qr_code_reader qr_code_reader.launch
~~~

Trial program
~~~
python3 main.py
~~~

## Files

 - `README.md`: Read me file (This file)

 - `main.py`: Read QR-code by using a image.

 - `main_ros.py`: Read QR-code by using a image message.

 - `generate_qr.py`:  Generate QR-code by input a sentense.

## References
