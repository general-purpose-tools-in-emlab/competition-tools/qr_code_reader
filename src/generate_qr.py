#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# 指定した文字列でQRを生成するプログラム

import qrcode
import cv2
from PIL import Image
from pyzbar.pyzbar import decode

# QRコードの作成
qr = qrcode.QRCode(box_size=15)
qr.add_data('Hello')
qr.make()
img_qr = qr.make_image()
img_qr.save('./data/test.png')



